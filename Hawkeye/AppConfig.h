//
//  AppConfig.h
//  Hawkeye
//
//  Created by WeKanCode on 16/08/16.
//  Copyright © 2016 WeKanCode. All rights reserved.
//

#import <Foundation/Foundation.h>

//build Environment for DEV, TEST, PROD

#define BUILD_ENVIRONMENT DEV
    //#define BUILD_ENVIRONMENT TEST
    //#define BUILD_ENVIRONMENT PROD

@interface AppConfig : NSObject

@end
